﻿namespace LaHorde.Exercices.GameLauncher.Services;
using System.Collections.Generic;

public class GameService
{
    public IEnumerable<Game> GetGames()
    {
        return new List<Game>
        {
            new()
            {
                Name = "Launchpad"
            },
            new() 
            {
                Name = "La horde game launcher" 
            }
        };
    }
}
