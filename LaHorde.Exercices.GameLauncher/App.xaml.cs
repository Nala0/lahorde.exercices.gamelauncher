﻿namespace LaHorde.Exercices.GameLauncher;
public partial class App : Application
{
    public App()
    {
        InitializeComponent();
        MainPage = new NavigationPage(new MainPage());
    }
}