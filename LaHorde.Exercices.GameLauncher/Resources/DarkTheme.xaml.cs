namespace LaHorde.Exercices.GameLauncher.Resources;

public partial class DarkTheme : ThemeDictionary
{
	public DarkTheme() : base("Dark")
	{
		InitializeComponent();
	}
}