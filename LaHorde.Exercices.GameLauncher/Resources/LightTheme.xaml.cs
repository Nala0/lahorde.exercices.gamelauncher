namespace LaHorde.Exercices.GameLauncher.Resources;

public partial class LightTheme : ThemeDictionary
{
	public LightTheme() : base("Light")
	{
		InitializeComponent();
	}
}