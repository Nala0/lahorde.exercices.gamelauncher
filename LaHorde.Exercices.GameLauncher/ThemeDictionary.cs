﻿namespace LaHorde.Exercices.GameLauncher
{
    public abstract class ThemeDictionary : ResourceDictionary
    {
        protected ThemeDictionary(string themeName)
        {
            ThemeName = themeName;
        }

        public string ThemeName { get; }
    }
}
